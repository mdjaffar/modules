help([[

Description
===========
R is a free software environment for statistical computing and graphics.


More information
================
 - Homepage: http://www.r-project.org/


Included extensions
===================
b-a, d-a, g-r, g-r, g-r, m-e, s-p, s-t, s-t, t-o, u-t
]])

whatis([[Description: R is a free software environment for statistical computing and graphics.]])
whatis([[Homepage: http://www.r-project.org/]])
whatis([[Extensions: b-a, d-a, g-r, g-r, g-r, m-e, s-p, s-t, s-t, t-o, u-t]])

local root = "/applis/PSMN/debian9/software/Core/R/3.2.4"

conflict("R")

load("GCC/7.2.0")

load("Java/1.8.0_151")

prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib/R/lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib/R/lib"))
prepend_path("MANPATH", pathJoin(root, "share/man"))
prepend_path("PATH", pathJoin(root, "bin"))
prepend_path("PKG_CONFIG_PATH", pathJoin(root, "lib/pkgconfig"))
setenv("EBROOTR", root)
setenv("EBVERSIONR", "3.2.4")
setenv("EBDEVELR", pathJoin(root, "easybuild/Core-R-3.2.4-easybuild-devel"))

-- Built with EasyBuild version 3.4.0
setenv("EBEXTSLISTR", "b-a,d-a,g-r,g-r,g-r,m-e,s-p,s-t,s-t,t-o,u-t")

-- PSMN: : 2.6.6.lua 2202 2018-03-30 09:00:33Z cpetit $
