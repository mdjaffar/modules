help([[

Description
===========
The GNU Compiler Collection includes front ends for C, C++, Objective-C, Fortran, Java, and Ada,
 as well as libraries for these languages (libstdc++, libgcj,...).


More information
================
 - Homepage: http://gcc.gnu.org/
]])

whatis([[Description: The GNU Compiler Collection includes front ends for C, C++, Objective-C, Fortran, Java, and Ada,
 as well as libraries for these languages (libstdc++, libgcj,...).]])
whatis([[Homepage: http://gcc.gnu.org/]])

local root = "/applis/PSMN/debian9/software/Core/GCC/7.2.0"

conflict("GCC")
prepend_path("MODULEPATH", "/applis/PSMN/debian9/Modules/all/Compiler/GCC/7.2.0")

prepend_path("CPATH", pathJoin(root, "include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib/gcc/x86_64-pc-linux-gnu/7.2.0"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib64"))
prepend_path("MANPATH", pathJoin(root, "share/man"))
prepend_path("PATH", pathJoin(root, "bin"))
setenv("CC", pathJoin(root, "bin/gcc"))
setenv("CXX", pathJoin(root, "bin/g++"))
setenv("FC", pathJoin(root, "bin/gfortran"))
setenv("EBROOTGCC", root)
setenv("EBVERSIONGCC", "7.2.0")
setenv("EBDEVELGCC", pathJoin(root, "easybuild/Core-GCC-7.2.0-easybuild-devel"))

-- Built with EasyBuild version 3.4.0

-- PSMN: : 2.6.6.lua 2202 2018-03-30 09:00:33Z cpetit $
