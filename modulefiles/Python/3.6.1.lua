help([[

Description
===========
Python is a programming language that lets you work more quickly and integrate your systems
 more effectively.


More information
================
 - Homepage: http://python.org/
]])

whatis([[Description: Python is a programming language that lets you work more quickly and integrate your systems
 more effectively.]])
whatis([[Homepage: http://python.org/]])

local root = "/applis/PSMN/debian9/software/MPI/GCC/7.2.0/OpenMPI/3.0.0/Python/3.6.1"

conflict("Python")

load("GCC/6.4.0")
load("GCC/6.4.0/OpenMPI/3.0.0")
load("GCC/6.4.0/OpenMPI/3.0.0/FFTW/3.3.6")

prepend_path("CPATH", pathJoin(root, "include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("MANPATH", pathJoin(root, "share/man"))
prepend_path("PATH", pathJoin(root, "bin"))
prepend_path("PKG_CONFIG_PATH", pathJoin(root, "lib/pkgconfig"))
setenv("EBROOTPYTHON", root)
setenv("EBVERSIONPYTHON", "3.6.1")
setenv("EBDEVELPYTHON", pathJoin(root, "easybuild/MPI-GCC-7.2.0-OpenMPI-3.0.0-Python-3.6.1-easybuild-devel"))

-- Built with EasyBuild version 3.4.0

-- PSMN: : 2.6.6.lua 2202 2018-03-30 09:00:33Z cpetit $
