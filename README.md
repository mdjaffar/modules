# LBMC module repository

## Description

This repository is the development of the LBMC module repository as
available on the PSMN in the /Xnfs/lbmcdb/common/modules/ folder.

If you don't want to contribute, you don't need to clone this repository and
you can proceed with the instruction of the **Installation** section.

## Installation

To get access to the LBMC modules repository use the following command:

```
ln -s /Xnfs/lbmcdb/common/modules/modulefiles ~/privatemodules
module use ~/privatemodules
```

You can add the following lines to your `~/.bashrc` or `~/.profile` file to
make this change permanent :

```sh
module use ~/privatemodules
```
