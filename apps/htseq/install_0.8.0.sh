#!/bin/bash
# Copyright laurent modolo for the LBMC UMR 5239 ©.
# contributor(s) : laurent modolo (2017)
#
# laurent.modolo@ens-lyon.fr
#
# This software is a computer program whose purpose is to manage dated file
# names in complience with the bioinformatic good practices used in the LBMC.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

source ../build_env.sh
module load Python/2.7.13

HTSEQ_VERSION=0.8.0
PYSAM_VERSION=0.11.2.2
HTSEQ_ROOT=/Xnfs/lbmcdb/common/modules/apps/HTSeq/${HTSEQ_VERSION}/
mkdir -p ${HTSEQ_VERSION}/lib/python2.7/dist-packages/
PYTHONPATH=$PYTHONPATH:${HTSEQ_ROOT}/lib/python2.7/dist-packages/

git clone https://github.com/pysam-developers/pysam.git
cd pysam
git checkout tags/v${PYSAM_VERSION}
python setup.py install --single-version-externally-managed --prefix ${HTSEQ_ROOT} --record pysam_install.txt
cd ..

git clone https://github.com/simon-anders/htseq.git
cd htseq
git checkout tags/release_${HTSEQ_VERSION}
python setup.py install --single-version-externally-managed --prefix ${HTSEQ_ROOT} --record htseq_install.txt
cd ..

rm -Rf htseq pysam
